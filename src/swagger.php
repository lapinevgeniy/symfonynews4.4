<?php

namespace App\Controller\swagger;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(title="API", version="0.1")
 * @OA\Server(
 *     url="http://symfony.loc/api",
 *     description="Api"
 * )
 */
