<?php

namespace App\Services;

use \App\Entity\News;
use App\Entity\Tag;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityNotFoundException;

class NewsService
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;

    public function __construct(NewsRepository $postRepository)
    {
        $this->newsRepository = $postRepository;
    }

    public function getNews(int $id)
    {
        return $this->newsRepository->findById($id);
    }

    public function getAllNews($filter = '')
    {
        return $this->newsRepository->findAll();
        //return $this->newsRepository->findAllQuery($filter);
    }

    public function addNews(string $title, string $text, string $tagTitle)
    {
        $news = new News();
        $news->setTitle($title);
        $news->setText($text);

        /*if($tagTitle) {
            $tag = new Tag();
            $tag->setTitle($tagTitle);
            $news->addTag($tag);
        }*/

        $this->newsRepository->save($news);

        return $news;
    }


    public function updateNews($id, $title, $text)
    {
        $news = $this->newsRepository->findById($id);

        if(!$news) {
            throw new EntityNotFoundException('news' . $id . 'not found');
        }
        $news->setTitle($title);
        $news->setText($text);
        $this->newsRepository->save($news);

        return $news;
    }

    public function deleteNews($id)
    {
        $news = $this->newsRepository->findById($id);

        if(!$news) {
            throw new EntityNotFoundException('news' . $id . 'not found');
        }
        $this->newsRepository->delete($news);

        return $news;
    }

}