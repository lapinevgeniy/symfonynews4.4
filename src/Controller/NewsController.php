<?php

namespace App\Controller;

use App\Services\NewsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use OpenApi\Annotations as OA;


/**
 * Class MovieApiController
 *
 */
class NewsController extends AbstractController
{
    /**
     * @var NewsService
     */
    private $newsService;

    public function __construct(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    /**
     * @Rest\Get("/news/{id}")
     * @param $id
     * @return View
     */
    public function getNews($id)
    {
        $news = $this->newsService->getNews($id);
        return View::create($news, Response::HTTP_OK);
    }

    /**
     * @Rest\QueryParam(name="filter")
     * @Rest\Get("/news")
     * @param $id
     * @return View
     */
    public function getNewsList(Request $request)
    {
        $filter = $request->get('filter');

        $news = $this->newsService->getAllNews($filter);

        return View::create($news, Response::HTTP_OK);
    }

    /**
     * @OA\Get(
     *     path="/news",
     *     @OA\Parameter(
     *          name="title",
     *          in="path",
     *          description="news title",
     *     ),
     *     @OA\Parameter(
     *          name="text",
     *          in="path",
     *          description="news text",
     *     ),
     *     @OA\Response(
     *          response="200",
     *          description="Succes",
     *          @OA\Schema(name="News", description="Sample model for the documentation")
     *     )
     * ),
     *     @OA\Response(response="206", description="Succes"),
     *     @OA\Response(response="400", description="Error"),
     *)
     * @param Request $request
     * @Rest\Post("/news")
     * @return View
     */
    public function postNews(Request $request)
    {
        #TODO Validation

        $news = $this->newsService->addNews(
            $request->get('title'),
            $request->get('text'),
            $request->get('tag')
        );

        return View::create($news, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Put("/news/{id}")
     * @param $id
     * @param Request $request
     * @return View
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function putNews($id, Request $request)
    {
        $news = $this->newsService->updateNews(
            $id,
            $request->get('title'),
            $request->get('text')
        );

        return View::create($news, Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/news/{id}")
     * @param $id
     * @return View
     * @throws \Doctrine\ORM\EntityNotFoundException
     */
    public function deleteNews($id)
    {
        $this->newsService->deleteNews($id);
        return View::create([], Response::HTTP_NO_CONTENT);
    }
}
