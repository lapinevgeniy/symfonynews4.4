<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    private $repository;

    /**
     * ArticleRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @param int $id
     * @return News|null
     */
    public function findById(int $id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @param $filter
     * @return mixed
     */
    public function findAllQuery($filter)
    {
        $qb = $this->createQueryBuilder('n')
            ->innerJoin('n.tags', 't');

        $serializer = SerializerBuilder::create()->build();
        return $qb->getQuery()->execute();
    }

    /**
     * @param News $post
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(News $post)
    {
        $this->_em->persist($post);
        $this->_em->flush();
    }

    /**
     * @param News $news
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(News $news)
    {
        $this->_em->remove($news);
        $this->_em->flush();
    }
}
